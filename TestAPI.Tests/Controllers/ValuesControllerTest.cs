﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestAPI.Controllers;
using TestAPI.Models;
using System.Net.Http;
using System.Web.Http;

namespace TestAPI.Tests.Controllers
{
    [TestClass]
    public class ValuesControllerTest
    {


        [TestMethod]
        public void Post()
        {
            // Arrange
            var controller = new ValuesController
            {
                Request = new HttpRequestMessage()
                {
                    Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
               }
           };

            #region "define test input"

            Image img1 = new Image();
            img1.showImage = "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg";

            Payload pl = new Payload();
            pl.drm = true;
            pl.image = img1; 
            pl.country = "UK";
            pl.episodeCount = 2;
            pl.genre = "Reality";
            pl.slug = "show/16kidsandcounting";
            pl.title = "GEM";

            Image img2= new Image();
            img2.showImage = "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg";
            Payload pl1 = new Payload();
            pl1.drm = true;
            pl1.image =img2 ;
            pl1.country = "USA";
            pl1.episodeCount = 2;
            pl1.genre = "Reality";
            pl1.slug = "Sea Patrol";
            pl1.title = "Channel 9";

            List<Payload> lst= new List<Payload>();

            lst.Add(pl);
            lst.Add(pl1);

            RootData rd = new RootData();
            rd.take = 10;
            rd.skip = 0;
            rd.totalRecords = 75;
            rd.payload = lst;  
            


            
            #endregion
            // Act
            var response = controller.Post(rd);

            // Assert
            
            Assert.AreEqual(HttpStatusCode.OK ,response.StatusCode );
        }

       
    }
}
