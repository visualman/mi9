﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Helpers;
using System.Web.Http.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using TestAPI.Controllers;
using TestAPI.Models;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json; 

namespace TestAPI.Tests.Controllers
{
    [TestClass]
    public class PayloadControllerTest
    {


        [TestMethod]
        public void Post()
        {
            // Arrange
            var controller = new PayloadController
            {
                Request = new HttpRequestMessage()
                {
                    Properties = { { HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration() } }
               }
           };

            #region "define test input"

            Image img = new Image();
            img.showImage = "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg";

            Payload pl = new Payload();
            pl.drm = true;
            pl.image = img; 
            pl.country = "UK";
            pl.episodeCount = 2;
            pl.genre = "Reality";
            pl.slug = "show/16kidsandcounting";
            pl.title = "GEM";

            Image img1= new Image();
            img1.showImage = "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg";
            Payload pl1 = new Payload();
            pl1.drm = false;
            pl1.image =img1 ;
            pl1.country = "USA";
            pl1.episodeCount = 2;
            pl1.genre = "Reality";
            pl1.slug = "Sea Patrol";
            pl1.title = "Channel 9";


            Image img2 = new Image();
            img2.showImage = "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg";
            Payload pl2 = new Payload();
            pl2.genre = "Reality";
            pl2.slug = "Sea Patrol";
            pl2.title = "Channel 9";

            Image img3 = new Image();
            img3.showImage = "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg";
            Payload pl3 = new Payload();
            pl3.drm = true;
            pl3.image = img3;
            pl3.country = "AUS";
            pl3.episodeCount = 2;
            pl3.genre = "Reality";
            pl3.slug = "Sea Patrol";
            pl3.title = "Channel 9";
            List<Payload> lst= new List<Payload>();

            lst.Add(pl);
            lst.Add(pl1);
            lst.Add(pl2);
            lst.Add(pl3);

            RootData rd = new RootData();
            rd.take = 10;
            rd.skip = 0;
            rd.totalRecords = 75;
            rd.payload = lst;  
            


            
            #endregion
            // Act
            var response = controller.Post(rd);
            
            // Assert
            var data=((System.Net.Http.ObjectContent) (response.Content)).Value;
            var Jobj = Json.Encode(data) ;

            JObject myJObject = JsonConvert.DeserializeObject<dynamic>(Jobj);
            JArray aObject = (JArray) myJObject["response"];
            
            Assert.AreEqual(HttpStatusCode.OK ,response.StatusCode );
            Assert.AreEqual(2,aObject.Count);

        }

       
    }
}
