
Overview
The APITest application show how you can use ASP.NET Web API for filtering json data based on custom criteria. 
-----------------------------------------------------------------------------------------------------------------------------
Software Requirment
This application developed on .Net Framework 4.5 using ASP.Net web api.
-----------------------------------------------------------------------------------------------------------------------------
Controllers
The PayloadController class implements the REST API that clients use to see filtered data:

REST API			Description	
-------------------------------------------------------------
POST /api/payload	the client sends a JSON input and get a JSON filtered list.
-------------------------------------------------------------

For the POST methods, the client sends the input as a JSON object:
{
    "payload": [
        {
            "country": "UK",
            "description": "What's life like when you have enough children to field your own football team?",
            "drm": true,
            "episodeCount": 3,
            "genre": "Reality",
            "image": {
                "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg"
            },
            "language": "English",
            "nextEpisode": null,
            "primaryColour": "#ff7800",
            "seasons": [
                {
                    "slug": "show/16kidsandcounting/season/1"
                }
            ],
            "slug": "show/16kidsandcounting",
            "title": "16 Kids and Counting",
            "tvChannel": "GEM"
        },...],
    "skip": 0,
    "take": 10,
    "totalRecords": 75
}

The POST methods return a JSON representation of the filtered data:
{
    "response": [
        {
            "image": "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg",
            "slug": "show/16kidsandcounting",
            "title": "16 Kids and Counting"
        },...]
}
-----------------------------------------------------------------------------------------------------------------------------
Models
The following table lists specific files and their basic purpose on models.
-InputData.cs
Used to model input data based on input JSON. The POST input parameter is defined in this class.

-FilteredData.cs
Usded to model output data and have a method for filtering data.

-----------------------------------------------------------------------------------------------------------------------------
Tests
-Using Fiddler:
Run Fiddler and go to composer set POST and enter Http://address/api/payload and enter input JSON in Requset body. 

-Using UnitTest in Visual Studio:
Open project in visual studion and run test from TEST/RUN/ALL TEST.
-----------------------------------------------------------------------------------------------------------------------------






