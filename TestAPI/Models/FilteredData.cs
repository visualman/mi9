﻿using System.Collections.Generic;
using System.Linq;

namespace TestAPI.Models
{
    public class FilteredData
    {
        public  string img { get; set; }
        public  string slug { get; set; }
        public  string title { get; set; }

        public static List<FilteredData> FilterInputData(RootData value)
        {
            
            var list = (from p in value.payload
                     where p.drm.HasValue && p.drm.Value && p.episodeCount > 0
                     select new FilteredData { img = p.image.showImage, slug = p.slug, title = p.title }); ;

            return list.AsEnumerable().Cast<FilteredData>().ToList();
        }
    }

    
}