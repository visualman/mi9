﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestAPI.Models;

namespace TestAPI.Controllers
{
    public class ValuesController : ApiController
    {
       

        // POST api/values
        public HttpResponseMessage Post([FromBody]RootData value)
        {
            if (!ModelState.IsValid)
            {
                 return Request.CreateResponse(HttpStatusCode.BadRequest,"\"error\": \"Could not decode request: JSON parsing failed\"");   
            }

            var output = FilteredData.FilterInputData( value);

            return Request.CreateResponse(HttpStatusCode.OK, output );
        }
    }
}