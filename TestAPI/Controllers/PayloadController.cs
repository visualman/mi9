﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestAPI.Models;

namespace TestAPI.Controllers
{
    public class PayloadController : ApiController
    {
       

        // POST api/values
        public HttpResponseMessage Post([FromBody]RootData value)
        {
            if (!ModelState.IsValid || value==null )
            {
                var error = new
                {

                    error = "Could not decode request: JSON parsing failed"

                };
                return Request.CreateResponse(HttpStatusCode.BadRequest, error);   
            }

            var output = FilteredData.FilterInputData( value);
            var collectionWrapper = new
            {

                response = output

            };

            return Request.CreateResponse(HttpStatusCode.OK, collectionWrapper);
        }
    }
}